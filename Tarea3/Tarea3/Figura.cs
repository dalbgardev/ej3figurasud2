﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea3
{
    /**
     *Bean encargado de la gestión de la clase Figura
     *@author David Albarrán García
     *@version 1.0
     */
    internal class Figura
    {
        /**
         * Método virtual perimetro
         * @return 0 float que devolverá las clases hijas
         */
        public virtual float Perimetro() { return 0; }
        /**
         * Método virtual area
         * @return 0 float que devolverá las clases hijas
         */
        public virtual float Area() { return 0; }
    }
}
