﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea3
{
    /**
     *Bean encargado de la gestión de la clase Triangulo, la cual hereda de Figura
     *@author David Albarrán García
     *@version 1.0
     */
    internal class Triangulo: Figura
    {
        /**
         * Propiedades base1 encargada de tener el valor de la base del triangulo
         * y altura encargada de tener el valor de la altura del triangulo
         */
        private float base1, altura;

        /**
         * Constructor encargado de asignarle los valores a las propiedades base1 y altura
         * @param float base1 
         * @param float altura
         */
        public Triangulo(float base1, float altura)
        {
            this.base1 = base1;
            this.altura = altura;
        }
        /**
         * Método sobreescrito de la clase padre Figura encargado de calcular el perímetro en base a las propiedades obtenidas por el constructor
         * primero calculando la hipotenusa y devolviendo el resultado de la suma de sus tres lados (se supone que es un triangulo rectángulo)
         * @return perimetro perimetro calculado
         */
        public override float Perimetro()
        {
            float hipotenusa = (float)(Math.Sqrt((base1 * base1) + (altura * altura)));

            float perimetro = base1 + altura + hipotenusa;

            return perimetro;
        }

        /**
         * Método sobreescrito de la clase padre Figura encargado de calcular el área del triangulo aplicando
         * la fórmula correspondiente.
         * @return (base1*altura/2) resultado del cálculo del área
         */
        public override float Area()
        {
            return (base1*altura/2);
        }
    }
}
