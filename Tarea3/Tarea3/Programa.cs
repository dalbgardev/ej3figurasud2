﻿using System;
using System.ComponentModel.Design;
using System.Timers;

namespace Tarea3
{
    /**
     * Bean encargado de todo el desarrollo de la aplicación
     *@author David Albarrán García
     *@version 1.0
     */
    internal class Programa
    {
        /**
         * Método principal el cual es llamado por la clase Principal y ejecuta el desarrollo llamando al
         * método menu.
        */
        internal void ejecutar()
        {
            menu();
        }
        /**
         * Método menú encargado de repetirse hasta que se introduzca la opción salir, el método se encarga
         * de mostrar el menú y solicitar al usuario la una opción, posterormente se llama al método 
         * ejecutarOpcion que devolvera un bool para salir en el caso de ser la opción elegida y se encargará
         * de validar y ejecutar la opción elegida.
         */
        private void menu()
        {
            bool salir = false;
            do
            {
                Console.WriteLine(
                "--MENÚ PRINCIPAL--\n" +
                "0 - Salir\n" +
                "1 - Rectángulo\n" +
                "2 - Círculo\n" +
                "3 - Triángulo\n"
                );

                Console.Write("Introduzca una opción: ");
                string opcion = Console.ReadLine();

                salir=ejecutarOpcion(opcion, salir);

            } while (!salir);
        }
        /**
         *Método que se encarga de la validación de las opciones y ejecutar lasopcines introducidas correctamente
         *el métdodo compruba que sea una opcion válida y avisará en el caso de que sea inválida, segun la opcion 
         *elegida desaarrollará los metodos correspondientes. Después de cada opción se llamará al método 
         *continuar que pedirá un intro y limpiará la pantalla.
         *@param string opcion la introducida por el usuario
         *@param bool salir propiedad salir para validar si la opcion elegida es salir.
         *@return bool salir devuelve salir, solo cambiara en el caso de introducir la opción 0
         */
        private bool ejecutarOpcion(string opcion, bool salir)
        {
            switch (opcion)
            {
                case "0":
                    salir = true;
                    break;
                case "1":
                    Rectangulo();
                    continuar();
                    break;
                case "2":
                    Circulo();
                    continuar();
                    break;
                case "3":
                    Triangulo();
                    continuar();
                    break;
                default:
                    Console.WriteLine("Opción incorrecta");
                    continuar();
                    break;
            }
            return salir;
        }

        /**
         * Método Triangulo encargado de pedir los datos correspondientes y validar que sean correctos,
         * en el caso de que sean correctos creará un nuevo triangulo con esos datos y mostrará su área
         * y perímetro, en el caso de que sean incorrecto lo informará
         */
        private void Triangulo()
        {
            try
            {
                Console.WriteLine("Introduzca la base: ");
                float base1 = float.Parse(Console.ReadLine());
                Console.WriteLine("Introduzca la altura: ");
                float altura = float.Parse(Console.ReadLine());

                Triangulo triangulo = new Triangulo(base1,altura);

                Console.WriteLine("El perímetro del triángulo es: " + triangulo.Perimetro());
                Console.WriteLine("El área del triángulo es: " + triangulo.Area());
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("Valor introducido nulo, no se puede coontinuar.");
            }
            catch (FormatException fe)
            {
                Console.WriteLine("El valor introducido no es un número");
            }
        }
        /**
         * Método Circulo encargado de pedir los datos correspondientes y validar que sean correctos,
         * en el caso de que sean correctos creará un nuevo circulo con esos datos y mostrará su área
         * y perímetro, en el caso de que sean incorrecto lo informará.
         */
        private void Circulo()
        {
            try
            {
                Console.WriteLine("Introduzca el radio: ");
                float radio = float.Parse(Console.ReadLine());

                Circulo circulo = new Circulo(radio);

                Console.WriteLine("El perímetro del círculo es: " + circulo.Perimetro());
                Console.WriteLine("El área del círculo es: " + circulo.Area());
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("Valor introducido nulo, no se puede coontinuar.");
            }
            catch (FormatException fe)
            {
                Console.WriteLine("El valor introducido no es un número");
            }
        }
        /**
         * Método encargado de pedir al usuario un intro para continuar, 
         * posteriormente se limpiará la pantalla.
         */
        private void continuar()
        {
            Console.Write("PULSA INTRO PARA CONTINUAR ");
            Console.ReadLine();
            Console.Clear();
        }
        /**
         * Método Rectangulo encargado de pedir los datos correspondientes y validar que sean correctos,
         * en el caso de que sean correctos creará un nuevo rectangulo con esos datos y mostrará su área
         * y perímetro, en el caso de que sean incorrecto lo informará.
         */
        private void Rectangulo()
        {
            try
            {
                Console.WriteLine("Introduzca el lado 1: ");
                float lado1 = float.Parse(Console.ReadLine());
                Console.WriteLine("Introduzca el lado 2: ");
                float lado2 = float.Parse(Console.ReadLine());

                Rectangulo rectangulo = new Rectangulo(lado1, lado2);

                Console.WriteLine("El perímetro del rectángulo es: " + rectangulo.Perimetro());
                Console.WriteLine("El área del rectángulo es: " + rectangulo.Area());
            }catch(ArgumentNullException ane) 
            {
                Console.WriteLine("Valor introducido nulo, no se puede coontinuar.");
            }catch (FormatException  fe)
            {
                Console.WriteLine("El valor introducido no es un número");
            }
        }
    }
}