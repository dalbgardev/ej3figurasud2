﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea3
{
    /**
     *Bean encargado de la gestión de la clase Rectangulo, la cual hereda de Figura
     *@author David Albarrán García
     *@version 1.0
     */
    internal class Rectangulo : Figura
    {
        /**
         * Propiedades lado1 encargado de tener el valor del lado1 del rectangulo
         * y lado2 encargado de tener el valor de el lado2 del rectangulo
         */
        private float lado1, lado2;
        /**
         * Constructor encargado de asignarle los valores a las propiedades lado1 y lado2
         * @param float lado1 
         * @param float lado2
         */
        public Rectangulo(float lado1, float lado2) 
        { 
            this.lado1 = lado1;
            this.lado2 = lado2;
        }
        /**
         * Método sobreescrito de la clase padre Figura encargado de calcular el perímetro en base a las propiedades obtenidas por el constructor
         * por la fórmula correspondiente
         * @return ((lado1*2)+(lado2*2)) perimetro calculado
         */
        public override float Perimetro()
        {
            return ((lado1*2)+(lado2*2));
        }
        /**
         * Método sobreescrito de la clase padre Figura encargado de calcular el área del Rectangulo aplicando
         * la fórmula correspondiente.
         * @return lado1*lado2 resultado del cálculo del área
         */
        public override float Area()
        {
            return lado1*lado2;
        }
    }
}
