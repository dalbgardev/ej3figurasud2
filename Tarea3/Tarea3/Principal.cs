﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea3
{
    /**
     * Bean encargado de la ejecución de la aplicación
     *@author David Albarrán García
     *@version 1.0
     */
    internal class Principal
    {
        /**
         * Main donde se crea un objeto programa y este objeto llama al método ejecutar para desarrollar la
         * aplicación, esto está sometido a un try catch para capturar errores inesperados.
         */
        static void Main(string[] args)
        {
            try
            {
                Programa programa = new Programa();
                programa.ejecutar();
            }catch (Exception ex)
            {
                Console.WriteLine("Error inesperado.");
            }
        }
    }
}
