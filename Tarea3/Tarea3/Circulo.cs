﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea3
{
    /**
     *Bean encargado de la gestión de la clase Circulo, la cual hereda de Figura
     *@author David Albarrán García
     *@version 1.0
     */
    internal class Circulo: Figura
    {
        /**
         * Propiedad radio encargada de tener el valor del radio del circulo
         */
        private float radio;
        /**
         * Constructor encargado de asignarle los valores a la propiedad radio
         * @param float radio 
         */
        public Circulo(float radio)
        {
            this.radio = radio;
        }
        /**
         * Método sobreescrito de la clase padre Figura encargado de calcular el perímetro en base a las propiedades obtenidas por el constructor
         * por la fórmula correspondiente
         * @return (float)(Math.PI*2*radio) perimetro calculado
         */
        public override float Perimetro()
        {
            return (float)(Math.PI*2*radio);
        }
        /**
         * Método sobreescrito de la clase padre Figura encargado de calcular el área del Círculo aplicando
         * la fórmula correspondiente.
         * @return (float)(Math.PI*Math.Pow(radio,2)) resultado del cálculo del área
         */
        public override float Area()
        {
            return (float)(Math.PI*Math.Pow(radio,2));
        }
    }
}
